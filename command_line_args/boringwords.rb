def boring?(line, boring_words)
  boring_words.any? { |w|
    line == w
  }
end

puts boring?("temp", ["temp", "recycler"]) 
puts boring?("/foo/bar", ["food", "bart", "quux"])