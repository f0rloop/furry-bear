# $boring_words = ["temp", "recycler"]

def check_usage
  unless ARGV.length == 2
	puts "Usage: ruby differences.rb filename.txt filename2.txt"
	exit
  end	
end

def boring?(line)
  contains?(line, 'temp') or contains?(line, 'recycler')
end

# def boring?(line, boring_words)
#   boring_words = $boring_words
#   boring_words.any? { |w|
#     contains?(line, w)
#   }
# end

def contains?(line, boring_word)
  line.chomp.split('/').include?(boring_word)
end 

def inventory_from(filename)
	inventory = File.open(filename)
	downcased = inventory.collect do |lines|
		lines.chomp.downcase
	end
	downcased.reject do |line|
		boring?(line)
	end
end

#TAKEAWAYS
# r = "recycler\n" notice the double quotes to interpret \n as new line 
# r.split('\\')    notice the double backslash, single backslash is dangerous
# r.chomp          to get rid of \n

def compare_inventory_files(old_file, new_file)
    old_inventory = inventory_from(old_file)
	new_inventory = inventory_from(new_file)

	puts "The following files have been added:" 
	puts new_inventory - old_inventory
	
	puts ""
	puts "The following files have been deleted:" 
	puts old_inventory - new_inventory	
end

if $0 == __FILE__ 
	check_usage
    compare_inventory_files(ARGV[0], ARGV[1])
end
