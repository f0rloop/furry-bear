##Ruby Drill
drill (drɪl) n.
Disciplined, repetitious exercise as a means of teaching and perfecting a skill or procedure.

###Objective:
Read a book, type the code, write your own, rinse, repeat.

###Feedback:
Email: mark@bashfulcoder.com