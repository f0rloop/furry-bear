#Ruby Arrays

puts [].size
new_array = %w(uno dos tres quatro) #expects string no need to put ' ' 
puts new_array.class

puts new_array[-1] #last element in array index

puts new_array[0..3] #inclusive range
puts new_array[0...3] #exclusive range

new_array.delete_at(0)
new_array.slice!(0..1)
puts "#{new_array} is the new array"

puts new_array.methods #list of methods in a nice way




