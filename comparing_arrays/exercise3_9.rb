# 1. The message length asks an array how long it is: irb(main):001:0> [1,2,3].length
# => 3
# Suppose you added this to your script:
#     x = (new_inventory - old_inventory).length
# What would be the value of x for the inventories new-inventory.txt and old-inventory.txt? 
# What information does x give you? What would be a better name than x?
old_inventory = File.open('list1.txt').readlines
new_inventory = File.open('list2.txt').readlines

x = (new_inventory - old_inventory).length # difference 
puts x
y = (old_inventory - new_inventory).length

# 2. What happens if you change the line in the previous exercise to this:
#     x = new_inventory - old_inventory.length
#  Why?
#z = new_inventory - old_inventory.length #(Not possible - TypeError)

# 3. Change the script so that it prints three additional pieces of information: 
# the number of files added in old-inventory.txt, 
# the number removed, and (the trickiest one) how many files were unchanged.
puts "The number of files added in old-inventory.txt is #{x}"
puts "The number of files removed are #{y}"
unchanged = (old_inventory & new_inventory).length
puts "The number of files unchanged are #{unchanged}"
# 4. Notice that both old-inventory.txt and new-inventory.txt are in alpha- betical order, so the arrays named by them are too. Does the script depend on that? Would it work if the inventories were scrambled?
# data types

